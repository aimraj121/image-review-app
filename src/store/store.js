import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
//import storeCardInfo from './modules/storeTicketInfo'
Vue.use(Vuex);

export const store = new Vuex.Store({
  strict: true,
  modules: {
    //storeCardInfo,
  },
  state: {
    isLoggedIn: false,
    user: {},
    user_role: "",
    selectedImg: "/images/E1NU5KU2.jpg",
    isFormReady: false,
    violationInfo: null
  },

  getters: {
    // getAccessToken: state => {
    //   return state.token;
    // },
    getLoggedInStatus: state => {
      return state.isLoggedIn;
    },
    getUser: state => {
      return state.user;
    },
    getUserRole: state => {
      return state.user_role;
    },
    getSelectedImg: state => {
      return state.selectedImg;
    },
    getFormStatus: state => {
      return state.isFormReady;
    },
    getViolationInfo: state => {
      return state.violationInfo;
    }
  },

  mutations: {
    setUserData(state, payload) {
      state.user = payload;
      // state.user_role = payload.access;
    },
    setUserRole(state, payload) {
      state.user_role = payload;
    },
    setLoginStatus(state, payload) {
      state.isLoggedIn = payload;
    },
    setSelectedImg(state, payload) {
      state.selectedImg = payload;
    },
    setFormStatus(state, payload) {
      state.isFormReady = payload;
    },
    setViolationInfo(state, payload) {
      state.violationInfo = payload;
    }
  },
  plugins: [createPersistedState()]
});
